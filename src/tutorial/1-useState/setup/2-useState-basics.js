import React, { useState } from 'react';

const UseStateBasics = () => {
  // console.log(useState('hello world'));
  // const value = useState(1)[0]
  // const handler = useState(1)[1]
  // console.log(value, handler);
  const [banana, setBanana] = useState('random title');
  const handleClick = () => {
    if (banana === 'random title') {
      setBanana('Hello useState');
    } else {
      setBanana('random title');
    }
  };
  return (
    <React.Fragment>
      <h1>{banana}</h1>
      <button type='button' className='btn' onClick={handleClick}>
        change title
      </button>
    </React.Fragment>
  );
};

export default UseStateBasics;
